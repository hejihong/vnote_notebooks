<h1 style='text-align: center'>Git 常用操作的命令组合</h1>

##### 1. 查看 Git 命令帮助文档
在需要查看 Git 命令的相关帮助文档时，有以下三种方式可以查看帮助文档
1. `man git <verb>`
    ```shell
    # 查看 git branch 的帮助文档
    man git branch
    ```

2. `git help <verb>`
    ```shell
    # 查看 git push 的帮助文档
    git help push
    ```

3. `git <verb> --help`
*该形式下的有些帮助命令会出现偏差，如：执行 `git branch --help` 时，会创建一个名为 `--help` 的分支，而非显示 `git branch` 的帮助文档。因此，在使用该形式的帮助命令时需要注意是否会引起偏差。*
    ```shell
    # 查看 git pull 的帮助文档
    git pull --help
    ```

----
  
##### 2. Git 的全局配置与单仓库配置
通常，在安装完 Git 后，要做的第一件事就是设置 `user.name` 和 `user.email` 签名信息。这些信息就是在 commit 时的签名，每次 commit 的记录中都会包含这些信息。值得注意的是，这些配置信息分为两类：<font color='red'>全局配置信息</font> 和 <font color='red'>单仓库配置信息</font>。存放配置信息的文件自然分为两个位置，全局配置信息的配置文件在用户根目录下，名为 `.gitconfig`，单仓库配置信息的配置文件在仓库根目录下的 `.git` 目录中，名为 `config`。二者对比如下：

|            |                                                  全局配置                                                  |                                             单仓库配置                                             |
| :--------: | :-------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------: |
|  配置文件名  |                                               `.gitconfig`                                                |                                             `config`                                              |
| 配置文件位置 |                                                 用户根目录                                                  |                                    仓库根目录下的 `.git` 文件夹下                                     |
| 配置文件作用 |               在单仓库中，若没有单仓库配置信息，则会默认使用全局配置信息。一般可以用于设置个人项目的配置信息                | 配置信息的优先级高于全局配置信息，可以用于管理非个人项目（如公司项目），指定特定的 `user.name` 和 `user.email` 即可 |
|  配置命令   | <font color='red'>git config --global <配置项> <配置值></font>，示例：`git config --global user.name "jihonghe"` |     <font color='red'>git config <配置项> <配置值></font>，示例：`git config user.name "jihonghe"`      |

<font color='red'>查看配置信息</font>：此处仅提一点，在仓库根目录下查看的配置信息是【全局配置】+【仓库配置】，会优先使用当前仓库配置。

----
  
##### 3. 仓库的创建与克隆
获取一个仓库有两种方式：<font color='red'>创建仓库</font>和<font color='red'>克隆仓库</font>。
**创建仓库**
```shell
# 1. 创建一个文件夹，作为仓库的根目录
mkdir myrepo
# 2. 创建并初始化仓库
git init
# 3. 创建完成后，可以查看 .git 文件夹信息
ls -la
```
**克隆仓库**：一般而言，执行 `git clone <git url>` 即可。关于 `<git url>`，Git 支持多种协议，所以 `<git url>` 可以是：`ssh://`，`git://`，`http(s)://`，本地协议等。其中，`git://` 下载速度最快，`ssh://` 用于需要用户认证的场合。
  
**`git init` 与 `git clone` 的简要对比说明**
关于 `git init` 和 `git clone` 的命令解析可以参考[易百教程](https://www.yiibai.com/git/git_init.html)，二者主要的区别有两方面：
1. `git init` 在初始化仓库后，尚未建立任何分支（master 分支也没有），这个可以使用 `git branch -a` 查看。要创建 master 分支，需要进行一次 `git commit` 才会生成；`git clone` 则会复制远程仓库中的所有已有分支。
2. `git init` 创建的仓库没有与远程仓库建立连接，执行额外的命令进行关联；`git clone` 获取的仓库会为本地仓库的每一个分支与远程仓库的分支建立关联（创建跟踪，可以使用 `git branch -r` 命令查看跟踪信息），同时会将 master 作为本地库的活动分支的初始分支。

----
  
##### 4. 简述工作区、暂存区、版本库、远程库
![Git分区关系图](_v_images/20190804173613563_687129546.png =780x)
四者关系如上图所示，下面一一解析各分区作用及分区间的相关操作。
- **Git 的本地数据管理**：工作区、暂存区和版本库
    - **工作区（Working Directory）**：工作区就是执行 `git init` 时所在的目录。工作区索要操作的文件就在该目录下，但不包括`.git`文件夹，`.git` 不属于工作区，它是对应的版本库（Repository）。
    - **暂存区（Index 或 Stage）**：暂存区用于记录从工作区 `git add` 过来的数据修改（增删改），它的作用在于作为工作区与版本库之间的一个过渡区（一方面使得提交更有条理，避免无用频繁琐碎的提交；另一方面是更加方便对文件进行版本管理）。暂存区实质就是 `.git` 文件夹下的 `index` 文件，执行 `git add` 时往暂存区存放的有两种信息：将本地文件的时间戳、长度，当前文档的对象id等信息保存到 `index`中。
    - **版本库（Commit History）**：版本库用于存放从暂存区 `git commit` 的数据，一次 `git commit` 就是一次新版本，执行 `git push` 时， push 的就是版本库的内容。另，版本库就是工作区中的 `.git` 文件。
- **版本库与远程库**
    - **关联版本库与远程库**：一般而言，通过 `git init` 创建的本地库需要通过命令与远程库进行关联。关联远程库可执行命令`git remote add origin <git_url>`，本地库与远程库建立了关联，但是本地分支并未与远程分支进行关联。因此，还需要执行相关命令进行分支关联。
- **待续……**

----
  
##### 5. 分支操作
- **查看本地分支与远程分支的映射关系**：`git branch -vv`
![查看本地分支与远程分支的关联关系](_v_images/20190805002650145_2143462015.png)
由上图可以看到，本地分支 master 与 远程分支 master 存在关联关系，而本地分支 hello 并没有与远程仓库的任何分支建立关联关系。此时，在执行命令 `git push` 和 `git pull` 时都是不成功的。
- **创建本地分支**
    - `git branch <local_br>`：创建新分支，但是不会自动切换到新创建的分支
    - `git checkout -b <local_br>`：创建并切换到新分支
- **将本地分支推送到远程分支（创建远程分支）**
    - `git push origin <local_br>`：该种推送方式不会与远程分支建立关联
    - `git push -u origin <local_br>`：与前者不同的是，该命令会将本地分支与推送后的远程同名分支建立关联
    - `git push -u origin <local_br>:<remote_br>`：与前者不同的是，该命令会将本地分支 local_br 推送到远程仓库名为 remote_br 的分支。也就是说，该远程分支名是可以自定义的。
- **本地分支与远程分支建立关联**：
    - `git branch --set-upstream-to=origin/<remote_br> <local_br>`：会将本地分支与<font color='red'>远程已有分支</font>建立关联
    - `git push -u origin local_br` 和 `git push -u origin <local_br>:<remote_br>`：二者在将本地分支推送到远程分支的同时，会自动关联远程分支，<font color='red'>该方式适用于要关联的远程分支不存在的情况</font>。
- **删除远程分支**
    - `git push origin :<remote_br>`：表示从本地推送一个空分支到指定的远程分支，也就是相当于删除远程分支
    - `git push origin --delete <remote_br>`：删除远程分支
